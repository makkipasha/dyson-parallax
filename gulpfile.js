const gulp = require('gulp');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const stripDebug = require('gulp-strip-debug');

const files = [
  'public/javascripts/ua-parser.min.js',
  'public/javascripts/lax.min.js',
  'public/javascripts/sequencor.js',
  'public/javascripts/controls.js',
];

gulp.task('packjs', function () {
    return gulp.src(files)
      .pipe(concat('all.js'))
      .pipe(stripDebug())
      .pipe(uglify())
      .pipe(gulp.dest('public/javascripts/'));
});

/*
gulp.task('default', function () {
  console.log('Hello World!');
});*/
//gulp.task('default', ['packjs']);

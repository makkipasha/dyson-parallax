/* Main controls for parallax webapp */
var canvas,ekyIntro,ctx,canvasWidth,canvasHeight,loadedHoovering=false,btnAR,pixelDensity=1,arDestination,arDisplayed=false,mobile=false;
window.onload = function() {
  // MOBILE DETECTION + WEBAR STUFF
  btnAR=document.getElementById("btn-webar");
  arDestination=document.querySelector("#ar-destination");
  var deviceParser=new UAParser(),result=deviceParser.getResult(),os=result.os.name;
  //console.log("Browser:" + JSON.stringify(result.browser));
  //console.log("Device:" + JSON.stringify(result.device));
  //console.log("OS:" + JSON.stringify(result.os));
  if(os=="iOS"||os=="Android"){
    document.body.classList.add("eky-mobile"); mobile=true;
    pixelDensity=2;
    if(btnAR!==null) {
      var destinationModel="intent://d15mv1adrb1s6e.cloudfront.net/product_bundles/GLB/12600021.glb?link=https%3A%2F%2Fd3nkfb7815bs43.cloudfront.net%2F%3Farid%3D12600021&title=#Intent;scheme=https;package=com.google.ar.core;action=android.intent.action.VIEW;S.browser_fallback_url=https%3A%2F%2Fd3nkfb7815bs43.cloudfront.net%2F%3Farid%3D12600021;end;";
      if(os=="iOS"){
        destinationModel="https://d15mv1adrb1s6e.cloudfront.net/product_bundles/USDZ/12600021.usdz#allowsContentScaling=0";
        arDestination.setAttribute("href",destinationModel); //arDestination.setAttribute("rel","ar");
        var quicklook_link = document.createElement("a");
        quicklook_link.setAttribute("href", destinationModel);
        quicklook_link.setAttribute("rel", "ar");
        quicklook_link.appendChild(document.createElement("img"));
        arDestination.addEventListener("click", function(ev) {
              ev.preventDefault();
              quicklook_link.click();
        });
      }else{
        arDestination.setAttribute("href",destinationModel);
        arDestination.setAttribute("target", "_self");
      }
      arDestination.style.display="none";
    }
  }else{
    if(btnAR!==null) {
      btnAR.addEventListener("click",function(){ document.getElementById("eky-video-overlay").style.display="flex";});
      arDestination.style.display="none";
    }
  }
  //DOM init, per section switch + sequencor init
  canvas = document.getElementById("sequence-canvas"); ctx = canvas.getContext("2d");
  ekyIntro = document.getElementById("eky-intro");
  ekyIntro.style.opacity = "0.5";
  updateCanvasSize(true);
  var skipSequenceStart=20700,skipSequenceEnd=23860,sequenceLength=1689;
  // document.getElementById("eky-main").style.height = (window.innerHeight + sequenceLength * 17) + 'px';
  if(section>1){
    ekyIntro.style.opacity = "0.0";
    skipSequenceStart=50000; skipSequenceEnd=60000; sequenceLength=1792;
    // document.getElementById("eky-main").style.height = (window.innerHeight + sequenceLength * 15) + 'px';
    // window.addEventListener("resize", function() {
    //   document.getElementById("eky-main").style.height = (window.innerHeight + sequenceLength * 15) + 'px';
    // });
  }else{
    // window.addEventListener("resize", function() {
    //   document.getElementById("eky-main").style.height = (window.innerHeight + sequenceLength * 17) + 'px';
    // });
    document.getElementById("eky-video-exit").addEventListener("click",function(){ document.getElementById("eky-video-overlay").style.display="none";});
    document.getElementById("eky-video-container").addEventListener("click",function(){ document.getElementById("eky-video-overlay").style.display="none";});
    updateLongRoom();
  }
  sequencor.init(skipSequenceStart,skipSequenceEnd,200,section,sequenceLength);

  //LAX init:
  lax.setup(); window.requestAnimationFrame(laxUpdate);

  //Window events:
  var w = window.innerWidth;
  // window.addEventListener("resize", function() {
  //   if(section<2) updateLongRoom();
  //   if(w !== window.innerWidth) lax.updateElements();
  //   updateCanvasSize(false);
  //   sequencor.update(canvasWidth,canvasHeight,-1);
  // });
  // window.onscroll = function(){
  //   sequencor.update(canvasWidth,canvasHeight);
  //   if(section<2){
  //     if(window.scrollY>18000 && !loadedHoovering){
  //       document.getElementById("eky-longroom").src="https://d2saxk2fepxz79.cloudfront.net/images/sequence3/long-room.jpg";
  //       var ekyWomenHoovering=document.querySelectorAll(".eky-hoovering-image");
  //       for(var i=1;i<=ekyWomenHoovering.length;i++) document.getElementById("eky-woman-hoovering-0"+i).src="https://d2saxk2fepxz79.cloudfront.net/images/sequence3/woman-hoovering-0"+i+".png";
  //       loadedHoovering=true;
  //     }
  //     if(window.scrollY>27000) {
  //       if(!arDisplayed) { arDisplayed=true; arDestination.style.display="block";}
  //     }else{
  //       if(arDisplayed) { arDisplayed=false; arDestination.style.display="none";}
  //     }
  //   }
  // };
}
// function laxUpdate(){ lax.update(window.scrollY); window.requestAnimationFrame(laxUpdate);}
// function updateLongRoom(){
//   var width=window.innerWidth,mult=2.732323;
//   if(window.innerWidth<576&&!mobile) mult=2.69;
//   document.getElementById("eky-longroom-container").setAttribute("data-lax-translate-x","0 0, 300 -"+(window.innerHeight*mult-window.innerWidth)+" | offset=-22800");
// }
function updateCanvasSize(init){
  canvasWidth=window.innerWidth*pixelDensity;
  canvasHeight=0.5625*canvasWidth*pixelDensity;
  ctx.canvas.width=canvasWidth;
  ctx.canvas.height=canvasHeight;
  if(!init) sequencor.update(window.scrollY);
}

/* SEQUENCOR An image sequence loader and parallax image player by Aurelien Prunier */

(function(window){ //TODO: resize window doesnt redraw image, it should, reset prev on resize
  function _sequencor(){
    var _sequencorObject = {},images=[],skipSequenceStart=0,skipSequenceEnd=0,lastImageLoaded=0,reset=false;
    var chunkSize=200,chunkLoaded=false,amountOfImagesLoadedInLastChunk=0,prev=-1,imageID=0,sequenceID,sequenceLength,skipScrollOffset;

    _sequencorObject.init = function(_skipSequenceStart,_skipSequenceEnd,_chunkSize,_sequenceID,_sequenceLength){
      chunkSize=_chunkSize; skipSequenceStart=_skipSequenceStart; skipSequenceEnd=_skipSequenceEnd;
      skipScrollOffset=skipSequenceEnd-skipSequenceStart; sequenceID=_sequenceID; sequenceLength=_sequenceLength;
      for(var i=0;i<=sequenceLength;i++) images.push("nada");
      this.loadImages(0);
    };

    _sequencorObject.update = function(_canvasWidth,_canvasHeight, newPrev){
      if(newPrev!==undefined) prev=newPrev;
      var scrollY=window.scrollY; //console.log("scroll: "+scrollY);
      if(scrollY<skipSequenceStart||scrollY>skipSequenceEnd) {
        var scrollOffset=0; if(scrollY>skipSequenceEnd) scrollOffset=skipScrollOffset;
        imageID=Math.floor((scrollY-scrollOffset)/15);
        if(chunkLoaded && imageID-(Math.floor(lastImageLoaded/chunkSize))*chunkSize>0.4*chunkSize) { //console.log("Load because over 40% at: "+imageID);
          chunkLoaded=false; amountOfImagesLoadedInLastChunk=0;
          this.loadImages(lastImageLoaded+1);
        }
        if(images[imageID]=="nada"){    //console.log("Load because current imageID NOT LOADED "+imageID);
          amountOfImagesLoadedInLastChunk=0;
          if(!chunkLoaded){ reset=true; //console.log("Still currently loading so resetting so that the next image stops its loop");
          }else{ chunkLoaded=false;     //console.log("Not currently loading so starting to load a chunk of images");
          }
          this.loadImages(imageID);
        }else if(images[imageID]!="load"){
          if(imageID!=prev&&imageID<=sequenceLength){   //console.log("Drawing image:"+imageID+" of seq:"+sequence+",result is: "+images[sequence][imageID]);
            ctx.drawImage(images[imageID],0,0,1280,720,0,0,_canvasWidth,_canvasHeight); prev=imageID;
          }
        }
      }
    };

    _sequencorObject.loadImages = function(start){
      if(start<=sequenceLength && images[start]=="nada"){ //console.log("LOADING image:"+start);
        images[start]=="load";
        var img = document.createElement('img');
        img.src="https://d2saxk2fepxz79.cloudfront.net/phase2/images/section"+sequenceID+"/section"+sequenceID+"-"+("000" + start).slice(-4)+".jpg"; //NORMAL CLOUDFRONT
        img.alt=start; //Passing the imageID into the alt tag to pick up later on image load
        img.addEventListener("load",function(e){
          var imageLoaded=Number(e.target.alt); //console.log("LOADED image:"+imageLoaded);
          images[imageLoaded]=this; lastImageLoaded=imageLoaded; amountOfImagesLoadedInLastChunk++;
          if(imageID==imageLoaded) {ctx.drawImage(images[imageID],0,0,1280,720,0,0,canvasWidth,canvasHeight); prev=imageID;}
          if(amountOfImagesLoadedInLastChunk<chunkSize){
            if(reset){ reset=false; }
            else{ imageLoaded++;_sequencorObject.loadImages(imageLoaded); }
          }else{
            var currentImageID=0,scrollo=window.scrollY; //These few lines are bit repetitive but i need to poll it all again to get value in real-time not when load event handler is added
            if(scrollo<skipSequenceStart||scrollo>skipSequenceEnd){
              var scrollOffset=0; if(scrollo>skipSequenceEnd) scrollOffset=skipScrollOffset;
              currentImageID = Math.floor((scrollo-scrollOffset)/15);
            }
            if(currentImageID > lastImageLoaded-chunkSize+chunkSize*.4) { //console.log("Load after chunk because over 40%!");
              amountOfImagesLoadedInLastChunk=0;
              _sequencorObject.loadImages(lastImageLoaded+1);
            }else{ //console.log("LOAD finished, last image loaded: "+lastImageLoaded);
              chunkLoaded=true;
            }
          }
        });
      }
    };
    return _sequencorObject;
  }
  if(typeof(window.sequencor) === 'undefined') window.sequencor = _sequencor(); // make library is globally accesible, then we save in the window
})(window);

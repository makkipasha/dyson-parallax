var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/section-1', function(req, res, next) {
  res.render('section-1', { title: 'Dyson Parallax' });
});

router.get('/section-2', function(req, res, next) {
  res.render('section-2', { title: 'Dyson Parallax' });
});

router.get('/section-screen', function(req, res, next) {
  res.render('section-screen', { title: 'Dyson Parallax' });
});
router.get('/section-cleans', function(req, res, next) {
  res.render('section-cleans', { title: 'Dyson Parallax' });
});
router.get('/section-dustdump', function(req, res, next) {
  res.render('section-dustdump', { title: 'Dyson Parallax' });
});
router.get('/section-dock', function(req, res, next) {
  res.render('section-dock', { title: 'Dyson Parallax' });
});
router.get('/section-carousel', function(req, res, next) {
  res.render('tools-carousel', { title: 'Dyson Parallax' });
});

router.get('/iframe', function(req, res, next) {
  res.render('democlamp', { title: 'Dyson Parallax' });
});

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Dyson Parallax' });
});

module.exports = router;
